# eimgui

[Dear ImGUI](https://github.com/ocornut/imgui) bridge for Erlang

> Dear ImGui is designed to enable fast iterations and to empower programmers to create content creation tools and visualization / debug tools (as opposed to UI for the average end-user). It favors simplicity and productivity toward this goal, and lacks certain features normally found in more high-level libraries.

ImGui or immediate mode GUI is a way of integrating GUI directly into the application without having separate world of widgets, events, mappings, and so on. Dear ImGUI provides all widgets as stateless functions, returning user interaction as a result.

Please check `example.erl` and [Dear ImGUI](https://github.com/ocornut/imgui) webpage for more details.

Build
-----

    $ rebar3 compile


Run example
-----

    $ example:run().


Naming
-----
Function names are derived from ImGui names by simply translating `CamelCase` to `snake_case`:

      ImGui::Checkbox => eimgui:checkbox
      ImGui::ListBox  => eimgui:list_box
      ImGui::InputTextMultiline => eimgui:input_text_multiline


Screenshot
-----

![Screenshot](https://i.imgur.com/mouebzL.png)


Some of these libs might be needed
-----

- cmd: sudo apt install libglew-dev
- cmd: sudo apt install freeglut3
- cmd: sudo apt install freeglut3-dev
- cmd: sudo apt install mesa-common-dev
- cmd: sudo apt install libglm-dev
- cmd: sudo apt install libglu-dev
