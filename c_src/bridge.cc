#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <erl_nif.h>

#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include "gl3w/GL/gl3w.h"    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include "glad.h"  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

static void glfw_error_callback(int error, const char* description)
{
  fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

struct color {
  int red;
  int green;
  int blue;
  double alpha;
};

GLFWwindow* global_window = NULL;

int init() {
  // Setup window
  glfwSetErrorCallback(glfw_error_callback);
  if (!glfwInit())
    return 1;

  // GL 3.0 + GLSL 130
  const char* glsl_version = "#version 130";
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
  //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

  // Create window with graphics context
  global_window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
  if (global_window == NULL)
    return 1;
  glfwMakeContextCurrent(global_window);
  glfwSwapInterval(1); // Enable vsync

  // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
  bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
  bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
  bool err = gladLoadGL() == 0;
#else
  bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
  if (err) {
    fprintf(stderr, "Failed to initialize OpenGL loader!\n");
    return 1;
  }

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  //ImGui::StyleColorsClassic();

  // Setup Platform/Renderer bindings
  ImGui_ImplGlfw_InitForOpenGL(global_window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  // Load Fonts
  // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
  // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
  // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
  // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
  // - Read 'misc/fonts/README.txt' for more instructions and details.
  // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
  //io.Fonts->AddFontDefault();
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
  //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
  //IM_ASSERT(font != NULL);

  /* bool show_demo_window = true; */
  /* bool show_another_window = false; */
  /* ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f); */
  return 0;
}


int list_as_string(ErlNifEnv* env, ERL_NIF_TERM term, char** result) {
  unsigned int length = 0;
  if (!enif_get_list_length(env, term, &length)) {
    return 0;
  }
  int size = 1 + sizeof(char)*length;
  char* buff = (char*) malloc(size);
  if (!enif_get_string(env, term, buff, size, ERL_NIF_LATIN1)) {
    return 0;
  }
  *result = buff;
  return 1;
}

int atom_as_string(ErlNifEnv* env, ERL_NIF_TERM term, char** result) {
  unsigned int length = 0;
  if (!enif_get_atom_length(env, term, &length, ERL_NIF_LATIN1)) {
    return 0;
  }
  int size = 1 + sizeof(char)*length;
  char* buff = (char*) malloc(size);
  if (!enif_get_atom(env, term, buff, size, ERL_NIF_LATIN1)) {
    return 0;
  }
  *result = buff;
  return 1;
}

int as_string(ErlNifEnv* env, ERL_NIF_TERM term, char** result) {
  if(enif_is_atom(env, term)) {
    return atom_as_string(env, term, result);
  }
  if(enif_is_list(env, term)) {
    return list_as_string(env, term, result);
  }
  return 0;
}

struct string_list {
  char **items;
  unsigned int size;
};

void free_string_list(struct string_list *list) {
  for(unsigned int i = 0; i < list->size; i++) {
    free(list->items[i]);
  }
  free(list->items);
}

int as_string_list(ErlNifEnv* env, ERL_NIF_TERM term, struct string_list *list) {
  unsigned int length = 0;
  
  if (!enif_get_list_length(env, term, &length)) {
    /* return enif_make_badarg(env); */
    return 0;
  }

  list->size = length;
  list->items = (char**) malloc(length * sizeof(char*));

  ERL_NIF_TERM head;
  for(unsigned int i = 0; i < length; i++) {
    char* item;
    if (!enif_get_list_cell(env, term, &head, &term) || !as_string(env, head, &item)) {
      /* free all filled elements */
      for(unsigned int k = 0; k < i; k++) {
        free(list->items[k]);
      }
      free(list->items);
      list->size = 0;
      return 0;
    }
    list->items[i] = item;
  }
  return 1;
}

int as_color(ErlNifEnv* env, ERL_NIF_TERM term, struct color *col) {
  const ERL_NIF_TERM *tuple;
  int arity;
  if(!enif_get_tuple(env, term, &arity, &tuple) || arity != 4) {
    return enif_make_badarg(env);
  }
  if(!enif_get_int(env, tuple[0], &(col->red))) {
    return enif_make_badarg(env);
  }
  if(!enif_get_int(env, tuple[1], &(col->green))) {
    return enif_make_badarg(env);
  }
  if(!enif_get_int(env, tuple[2], &(col->blue))) {
    return enif_make_badarg(env);
  }
  if(!enif_get_double(env, tuple[3], &(col->alpha))) {
    return enif_make_badarg(env);
  }
  return 1;
}

struct state { };

void free_state(state* state) {
  free(state);
}

// Erlang NIFS

static ERL_NIF_TERM same_line_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::SameLine();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM push_item_width_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  int width;
  if(!enif_get_int(env, argv[0], &width)) {
    return enif_make_badarg(env);
  }
  ImGui::PushItemWidth(width);
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM pop_item_width_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::PopItemWidth();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM button_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }
  /* state* gstate = (state*) enif_priv_data(env); */
  int ret = ImGui::Button(label);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM text_colored_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  struct color col;
  if(!as_color(env, argv[1], &col)) {
    return enif_make_badarg(env);
  }

  ImVec4 vec(col.red, col.green, col.blue, col.alpha);
  ImGui::TextColored(vec, "%s", label);
  
  int ret = 0;
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM text_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  ImGui::Text("%s", label);
  int ret = 0;
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM input_text_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }
  char *value;
  if(!as_string(env, argv[1], &value)) {
    return enif_make_badarg(env);
  }
  int max_size;
  if(!enif_get_int(env, argv[2], &max_size)) {
    return enif_make_badarg(env);
  }

  char *buffer = (char*) malloc(sizeof(char) * max_size);
  strcpy(buffer, value);
  
  ImGui::InputText(label, buffer, max_size);
  ERL_NIF_TERM ret = enif_make_string(env, buffer, ERL_NIF_LATIN1);
  free(buffer);
  return ret;
}

static ERL_NIF_TERM input_text_multiline_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }
  char *value;
  if(!as_string(env, argv[1], &value)) {
    return enif_make_badarg(env);
  }

  int height;
  if(!enif_get_int(env, argv[2], &height)) {
    return enif_make_badarg(env);
  }

  int max_size;
  if(!enif_get_int(env, argv[3], &max_size)) {
    return enif_make_badarg(env);
  }

  int flags;
  if(!enif_get_int(env, argv[4], &flags)) {
    return enif_make_badarg(env);
  }

  int len = strlen(value);
  if(len > max_size) {
    max_size = len;
  }

  char *buffer = (char*) malloc(sizeof(char) * (max_size+1));
  strcpy(buffer, value);

  ImVec2 h(0, ImGui::GetTextLineHeight() * height);
  ImGui::InputTextMultiline(label, buffer, max_size, h, flags);
  ERL_NIF_TERM ret = enif_make_string(env, buffer, ERL_NIF_LATIN1);
  free(buffer);
  return ret;
}

static ERL_NIF_TERM list_box_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  struct string_list list;
  if(!as_string_list(env, argv[1], &list)) {
    return enif_make_badarg(env);
  }

  int selected;
  if(!enif_get_int(env, argv[2], &selected)) {
    return enif_make_badarg(env);
  }

  int display_size;
  if(!enif_get_int(env, argv[3], &display_size)) {
    return enif_make_badarg(env);
  }
  
  /* state* gstate = (state*) enif_priv_data(env); */
  ImGui::ListBox(label, &selected, list.items, list.size, display_size);
  int ret = selected;
  free(label);
  free_string_list(&list);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM checkbox_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  int selected;
  if(!enif_get_int(env, argv[1], &selected)) {
    return enif_make_badarg(env);
  }
  
  bool check = (bool) selected;
  ImGui::Checkbox(label, &check);
  int ret = (int) check;
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM radio_button_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  int selected;
  if(!enif_get_int(env, argv[1], &selected)) {
    return enif_make_badarg(env);
  }

  int value;
  if(!enif_get_int(env, argv[2], &value)) {
    return enif_make_badarg(env);
  }
  
  /* state* gstate = (state*) enif_priv_data(env); */
  ImGui::RadioButton(label, &selected, value);
  int ret = selected;
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM collapsing_header_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  /* state* gstate = (state*) enif_priv_data(env); */
  int ret = ImGui::CollapsingHeader(label);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM tree_node_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  /* state* gstate = (state*) enif_priv_data(env); */
  int ret = ImGui::TreeNode(label);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM tree_pop_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::TreePop();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM set_scroll_here_y_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  double center;
  if(!enif_get_double(env, argv[0], &center)) {
    return enif_make_badarg(env);
  }
  ImGui::SetScrollHereY((float)center);
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM begin_tab_bar_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  int flags = 0;
  if(!enif_get_int(env, argv[1], &flags)) {
    return enif_make_badarg(env);
  }

  int ret = ImGui::BeginTabBar(label, flags);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM end_tab_bar_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::EndTabBar();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM begin_tab_item_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }

  int ret = ImGui::BeginTabItem(label);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM end_tab_item_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::EndTabItem();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM begin_group_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::BeginGroup();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM end_group_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::EndGroup();
  int ret = 0;
  return enif_make_int(env, ret);
}


static ERL_NIF_TERM begin_window_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  char *label;
  if(!as_string(env, argv[0], &label)) {
    return enif_make_badarg(env);
  }
  
  int flags = 0;
  if(!enif_get_int(env, argv[1], &flags)) {
    return enif_make_badarg(env);
  }

  bool show = 1;
  int ret = ImGui::Begin(label, &show, flags);
  free(label);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM end_window_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  ImGui::End();
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM init_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  int ret = init();
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM shutdown_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  // Cleanup
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(global_window);
  glfwTerminate();
  
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM start_frame_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  glfwPollEvents();
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();
  
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM end_frame_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{

  ImGui::Render();
  int display_w, display_h;
  glfwMakeContextCurrent(global_window);
  glfwGetFramebufferSize(global_window, &display_w, &display_h);
  glViewport(0, 0, display_w, display_h);
  /* glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w); */
  glClear(GL_COLOR_BUFFER_BIT);
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

  glfwMakeContextCurrent(global_window);
  glfwSwapBuffers(global_window);
  
  int ret = 0;
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM window_should_close_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  int ret = glfwWindowShouldClose(global_window);
  return enif_make_int(env, ret);
}

static ERL_NIF_TERM demo_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  bool show_demo_window = true;
  ImGui::ShowDemoWindow(&show_demo_window);
  int ret = 0;
  return enif_make_int(env, ret);
}

static ErlNifFunc nif_funcs[] = {
                                 // widgets
                                 {"button", 1, button_nif},
                                 {"text", 1, text_nif},
                                 {"text_colored", 2, text_colored_nif},
                                 {"input_text", 3, input_text_nif},
                                 {"input_text_multiline", 5, input_text_multiline_nif},
                                 {"list_box", 4, list_box_nif},
                                 {"checkbox", 2, checkbox_nif},
                                 {"radio_button", 3, radio_button_nif},
                                 {"collapsing_header", 1, collapsing_header_nif},
                                 {"tree_node", 1, tree_node_nif},
                                 {"tree_pop", 0, tree_pop_nif},

                                 // Tabs
                                 {"begin_tab_bar", 2, begin_tab_bar_nif},
                                 {"end_tab_bar", 0, end_tab_bar_nif},
                                 {"begin_tab_item", 1, begin_tab_item_nif},
                                 {"end_tab_item", 0, end_tab_item_nif},

                                 {"begin_window", 2, begin_window_nif},
                                 {"end_window", 0, end_window_nif},

                                 // layout
                                 {"same_line", 0, same_line_nif},
                                 {"push_item_width", 1, push_item_width_nif},
                                 {"pop_item_width",  0, pop_item_width_nif},
                                 {"set_scroll_here_y",  1, set_scroll_here_y_nif},

                                 {"begin_group", 0, begin_group_nif},
                                 {"end_group", 0, end_group_nif},

                                 // infra
                                 {"init", 0, init_nif},
                                 {"shutdown", 0, shutdown_nif},
                                 {"start_frame", 0, start_frame_nif},
                                 {"end_frame", 0, end_frame_nif},
                                 {"window_should_close", 0, window_should_close_nif},

                                 {"demo", 0, demo_nif}
};

int load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM load_info) {
  state* gstate = (state*)malloc(sizeof(struct state));
  *priv_data = gstate;
  return 0;
}

int upgrade(ErlNifEnv* env, void** priv_data, void** old_priv_data, ERL_NIF_TERM load_info) {
  *priv_data = *old_priv_data;
  return 0;
}

void unload(ErlNifEnv* env, void* priv_data) {
  free_state((state*) priv_data);
  return;
}

ERL_NIF_INIT(eimgui, nif_funcs, load, NULL, upgrade, unload)
