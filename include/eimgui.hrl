%% Window flags

-define(ImGuiWindowFlags_None                      , 0).
-define(ImGuiWindowFlags_NoTitleBar                , 1 bsl 0).
-define(ImGuiWindowFlags_NoResize                  , 1 bsl 1).
-define(ImGuiWindowFlags_NoMove                    , 1 bsl 2).
-define(ImGuiWindowFlags_NoScrollbar               , 1 bsl 3).
-define(ImGuiWindowFlags_NoScrollWithMouse         , 1 bsl 4).
-define(ImGuiWindowFlags_NoCollapse                , 1 bsl 5).
-define(ImGuiWindowFlags_AlwaysAutoResize          , 1 bsl 6).
-define(ImGuiWindowFlags_NoBackground              , 1 bsl 7).
-define(ImGuiWindowFlags_NoSavedSettings           , 1 bsl 8).
-define(ImGuiWindowFlags_NoMouseInputs             , 1 bsl 9).
-define(ImGuiWindowFlags_MenuBar                   , 1 bsl 10).
-define(ImGuiWindowFlags_HorizontalScrollbar       , 1 bsl 11).
-define(ImGuiWindowFlags_NoFocusOnAppearing        , 1 bsl 12).
-define(ImGuiWindowFlags_NoBringToFrontOnFocus     , 1 bsl 13).
-define(ImGuiWindowFlags_AlwaysVerticalScrollbar   , 1 bsl 14).
-define(ImGuiWindowFlags_AlwaysHorizontalScrollbar , 1 bsl 15).
-define(ImGuiWindowFlags_AlwaysUseWindowPadding    , 1 bsl 16).
-define(ImGuiWindowFlags_NoNavInputs               , 1 bsl 18).
-define(ImGuiWindowFlags_NoNavFocus                , 1 bsl 19).
-define(ImGuiWindowFlags_UnsavedDocument           , 1 bsl 20).
-define(ImGuiWindowFlags_NoNav,
  ?ImGuiWindowFlags_NoNavInputs
  bor ?ImGuiWindowFlags_NoNavFocus
).
-define(ImGuiWindowFlags_NoDecoration,
  ?ImGuiWindowFlags_NoTitleBar
  bor ?ImGuiWindowFlags_NoResize
  bor ?ImGuiWindowFlags_NoScroll
  bor ?ImGuiWindowFlags_NoCollapse
).
-define(ImGuiWindowFlags_NoInputs,
  ?ImGuiWindowFlags_NoMouseInputs
  bor ?ImGuiWindowFlags_NoNavInputs
  bor ?ImGuiWindowFlags_NoNavFocus
).

-define(ImGuiWindowFlags_NavFlattened, 1 bsl 23).

%% Input text flags

-define(ImGuiInputTextFlags_None, 0).
-define(ImGuiInputTextFlags_CharsDecimal, 1 bsl 0).
-define(ImGuiInputTextFlags_CharsHexadecimal, 1 bsl 1).
-define(ImGuiInputTextFlags_CharsUppercase, 1 bsl 2).
-define(ImGuiInputTextFlags_CharsNoBlank, 1 bsl 3).
-define(ImGuiInputTextFlags_AutoSelectAll, 1 bsl 4).
-define(ImGuiInputTextFlags_EnterReturnsTrue, 1 bsl 5).
-define(ImGuiInputTextFlags_CallbackCompletion, 1 bsl 6).
-define(ImGuiInputTextFlags_CallbackHistory, 1 bsl 7).
-define(ImGuiInputTextFlags_CallbackAlways, 1 bsl 8).
-define(ImGuiInputTextFlags_CallbackCharFilter, 1 bsl 9).
-define(ImGuiInputTextFlags_AllowTabInput, 1 bsl 10).
-define(ImGuiInputTextFlags_CtrlEnterForNewLine, 1 bsl 11).
-define(ImGuiInputTextFlags_NoHorizontalScroll, 1 bsl 12).
-define(ImGuiInputTextFlags_AlwaysInsertMode, 1 bsl 13).
-define(ImGuiInputTextFlags_ReadOnly, 1 bsl 14).
-define(ImGuiInputTextFlags_Password, 1 bsl 15).
-define(ImGuiInputTextFlags_NoUndoRedo, 1 bsl 16).
-define(ImGuiInputTextFlags_CharsScientific, 1 bsl 17).
-define(ImGuiInputTextFlags_CallbackResize, 1 bsl 18).

%% Tree node flags

-define(ImGuiTreeNodeFlags_None, 0).
-define(ImGuiTreeNodeFlags_Selected, 1 bsl 0).
-define(ImGuiTreeNodeFlags_Framed, 1 bsl 1).
-define(ImGuiTreeNodeFlags_AllowItemOverlap, 1 bsl 2).
-define(ImGuiTreeNodeFlags_NoTreePushOnOpen, 1 bsl 3).
-define(ImGuiTreeNodeFlags_NoAutoOpenOnLog, 1 bsl 4).
-define(ImGuiTreeNodeFlags_DefaultOpen, 1 bsl 5).
-define(ImGuiTreeNodeFlags_OpenOnDoubleClick, 1 bsl 6).
-define(ImGuiTreeNodeFlags_OpenOnArrow, 1 bsl 7).
-define(ImGuiTreeNodeFlags_Leaf, 1 bsl 8).
-define(ImGuiTreeNodeFlags_Bullet, 1 bsl 9).
-define(ImGuiTreeNodeFlags_FramePadding, 1 bsl 10).
-define(ImGuiTreeNodeFlags_NavLeftJumpsBackHere, 1 bsl 13).
-define(ImGuiTreeNodeFlags_CollapsingHeader,
  ?ImGuiTreeNodeFlags_Framed
  bor ?ImGuiTreeNodeFlags_NoTreePushOnOpen
  bor ?ImGuiTreeNodeFlags_NoAutoOpenOnLog
).
