-module(eimgui).

%% API exports / wrappers
-export([
  input_text/2,
  input_text_multiline/3,
  input_text_multiline/4
]).

%% NIF exports
-export([
  init/0,
  shutdown/0,
  begin_window/2,
  end_window/0,
  start_frame/0,
  end_frame/0,
  window_should_close/0,
  demo/0,

  button/1,
  text/1,
  text_colored/2,
  input_text/3,
  input_text_multiline/5,
  list_box/4,
  checkbox/2,
  radio_button/3,

  begin_tab_bar/2,
  end_tab_bar/0,
  begin_tab_item/1,
  end_tab_item/0,

  collapsing_header/1,
  tree_node/1,
  tree_pop/0,
  set_scroll_here_y/1,

  same_line/0,
  push_item_width/1,
  pop_item_width/0,

  begin_group/0,
  end_group/0
]).

-on_load(init_nif/0).

%%====================================================================
%% API functions
%%====================================================================


%%====================================================================
%% Internal functions
%%====================================================================
init_nif() ->
  Priv = code:priv_dir(eimgui),
  ok = erlang:load_nif(Priv ++ "/imgui", 0).

init() ->
  exit(nif_library_not_loaded).

shutdown() ->
  exit(nif_library_not_loaded).

begin_window(_Id, _Flags) ->
  exit(nif_library_not_loaded).

end_window() ->
  exit(nif_library_not_loaded).

start_frame() ->
  exit(nif_library_not_loaded).

end_frame() ->
  exit(nif_library_not_loaded).

window_should_close() ->
  exit(nif_library_not_loaded).

%% Widgets

button(_Id) ->
  exit(nif_library_not_loaded).

text(_Id) ->
  exit(nif_library_not_loaded).

text_colored(_Id, _Color) ->
  exit(nif_library_not_loaded).

input_text(Id, Text) ->
  input_text(Id, Text, 8196).

input_text(_Id, _Text, _Size) ->
  exit(nif_library_not_loaded).

input_text_multiline(Id, Text, Height) ->
  input_text_multiline(Id, Text, Height, 0).

input_text_multiline(Id, Text, Height, Flags) ->
  input_text_multiline(Id, Text, Height, 8196*2, Flags).

input_text_multiline(_Id, _Text, _Height, _Size, _Flags) ->
  exit(nif_library_not_loaded).

list_box(_Id, _Items, _Current, _Display) ->
  exit(nif_library_not_loaded).

checkbox(_Id, _Selected) ->
  exit(nif_library_not_loaded).

radio_button(_Id, _Selected, _Value) ->
  exit(nif_library_not_loaded).

collapsing_header(_Id) ->
  exit(nif_library_not_loaded).

tree_node(_Id) ->
  exit(nif_library_not_loaded).

tree_pop() ->
  exit(nif_library_not_loaded).

%% Tabs
begin_tab_bar(_Id, _Flags) ->
  exit(nif_library_not_loaded).

end_tab_bar() ->
  exit(nif_library_not_loaded).

begin_tab_item(_Id) ->
  exit(nif_library_not_loaded).

end_tab_item() ->
  exit(nif_library_not_loaded).

%% Layout

same_line() ->
  exit(nif_library_not_loaded).

push_item_width(_Width) ->
  exit(nif_library_not_loaded).

pop_item_width() ->
  exit(nif_library_not_loaded).

set_scroll_here_y(_Center) ->
  exit(nif_library_not_loaded).

begin_group() ->
  exit(nif_library_not_loaded).

end_group() ->
  exit(nif_library_not_loaded).

demo() ->
  exit(nif_library_not_loaded).
