-module(example).

-export([
  run/0,
  init/1,
  shutdown/1,
  render/1,
  handle_message/2
]).

-include("eimgui.hrl").

run() ->
  gen_gui:start_link(?MODULE, #{}).

init(_) ->
  #{
    module_selected => 0,
    item_selected => 0,
    radio_selected => 0,
    checked => 0,
    line_input => "write here",
    multi_input => "wiret here too"
  }.

render(State) ->
  #{
    module_selected := Module0,
    item_selected := Item0,
    radio_selected := Radio0,
    checked := Checked0,
    line_input := LInput0,
    multi_input := MInput0
  } = State,

  %% eimgui:demo(),

  case eimgui:button("generate error") of
    1 -> 1 / 0;
    _ -> ok
  end,

  Item = eimgui:list_box("items small", [a, b, c, d], Item0, 3),
  Modules = [F || {F, _} <- code:all_loaded()],
  MEvents = gen_gui:recv_events(modules),
  Module01 = case lists:member(select_random, MEvents) of
    true -> rand:uniform(length(Modules));
    false -> Module0
  end,

  eimgui:set_scroll_here_y(0.5),
  Module = eimgui:list_box(loaded_modules, Modules, Module01, 10),
  eimgui:set_scroll_here_y(0.5),

  case eimgui:button("select random") of
    1 -> gen_gui:send_event(modules, select_random);
    _ -> ok
  end,

  Checked = eimgui:checkbox(items2, Checked0),

  eimgui:begin_group(),
  Rad1 = eimgui:radio_button(radioA, Radio0, 0),
  Rad2 = eimgui:radio_button(radioB, Radio0, 1),
  Rad3 = eimgui:radio_button(radioC, Radio0, 2),

  Radio = case [X || X <- [Rad1, Rad2, Rad3], X /= Radio0] of
    [R] -> R;
    []  -> Radio0
  end,
  eimgui:end_group(),

  eimgui:same_line(),
  eimgui:begin_group(),
  Radio1 = case eimgui:begin_tab_bar("tabs", 0) of
    1 ->
      Rd1 = case eimgui:begin_tab_item("tab1") of
        1 ->
          eimgui:text("tab1 text"),
          eimgui:end_tab_item(),
          0;
        _ -> Radio
      end,

      Rd2 = case eimgui:begin_tab_item("tab2") of
        1 ->
          eimgui:text("tab2 text"),
          eimgui:end_tab_item(),
          1;
        _ -> Radio
      end,

      Rd3 = case eimgui:begin_tab_item("tab3") of
        1 ->
          eimgui:text("tab3 text"),
          eimgui:end_tab_item(),
          2;
        _ -> Radio
      end,
      eimgui:end_tab_bar(),
      case [X || X <- [Rd1, Rd2, Rd3], X /= Radio] of
        [] -> Radio;
        [Y] -> Y
      end;
    _ ->
      Radio
  end,
  eimgui:end_group(),

  eimgui:begin_window("window 2 (not resizable)", ?ImGuiWindowFlags_NoResize bor ?ImGuiWindowFlags_AlwaysAutoResize),
  %% eimgui:begin_window("window 2 (not resizable)", 0),
  eimgui:text("hello from window 2"),
  eimgui:push_item_width(100),
  Input = eimgui:input_text("small input", LInput0),

  eimgui:text_colored(Input, {0, 0, 255, 1.0}),
  Input2 = eimgui:input_text_multiline("input2", MInput0, 5),
  eimgui:text_colored(Input2, {255, 0, 255, 1.0}),
  eimgui:pop_item_width(),
  case eimgui:collapsing_header("Header1") of
    1 ->
      case eimgui:tree_node("Subnode1") of
        1 -> eimgui:tree_pop();
        _ -> ok
      end,
      case eimgui:tree_node("Subnode2") of
        1 -> eimgui:tree_pop();
        _ -> ok
      end;
    _ ->
      ok
  end,
  case eimgui:collapsing_header("Header2") of
    1 -> eimgui:text("hello under header2");
    _ -> ok
  end,

  eimgui:end_window(),

  State#{
    module_selected => Module,
    item_selected   => Item,
    radio_selected  => Radio,
    checked         => Checked,
    line_input      => Input,
    multi_input     => Input2
  }.

shutdown(_) ->
  ok.

handle_message(_Msg, State) ->
  {ok, State}.
