-module(gen_gui).

-export([
  start_link/2,
  send_event/2,
  recv_events/1,
  init/2
]).

-define(EVENTS_TAB, '$gui_events').
-define(FRAME_ID_KEY, '$gui_frame_id').

%%====================================================================
%% API functions
%%====================================================================

start_link(Mod, Args) ->
  proc_lib:spawn_link(?MODULE, init, [Mod, Args]).

-spec send_event(term(), any()) -> true.
send_event(Tag, Data) ->
  Tab = get(?EVENTS_TAB),
  ets:insert(Tab, {Tag, frame_id()+1, Data}).

-spec recv_events(term()) -> [any()].
recv_events(Tag) ->
  Tab = get(?EVENTS_TAB),
  ets:select(Tab, [{{Tag, frame_id(), '$1'}, [], ['$1']}]).

%%====================================================================
%% Internal functions
%%====================================================================
init(Mod, Args) ->
  UserState = Mod:init(Args),
  process_flag(trap_exit, true),
  EventsTab = ets:new(?MODULE, [protected, bag]),
  put(?EVENTS_TAB, EventsTab),
  put(?FRAME_ID_KEY, 0),
  eimgui:init(),
  loop(state(Mod, UserState)).

state(UserMod, UserState) ->
  #{
    should_close => eimgui:window_should_close(),
    errors       => [],
    errors_limit => 300,
    user_mod     => UserMod,
    user_state   => UserState
  }.

update_state(#{should_close := 1}=State) ->
  State;

update_state(#{errors := E, errors_limit := Limit}=State) ->
  Errors = case length(E) > Limit of
    true -> lists:sublist(E, Limit);
    _ -> E
  end,
  State#{
    errors => Errors,
    should_close => eimgui:window_should_close()
  }.

loop(#{should_close := 0}=State) ->
  increase_frame_id(),
  clear_events(),
  #{user_mod := UMod, user_state := UState} = State,
  eimgui:start_frame(),
  State1 = try
             UState2 = UMod:render(UState),
             State#{user_state => UState2}
           catch Type:Msg ->
             Trace = erlang:get_stacktrace(),
             report_error(Type, Msg, Trace, State)
           end,
  display_errors(maps:get(errors, State)),
  eimgui:end_frame(),
  State2 = receive_message(State1),
  loop(update_state(State2));

loop(State) ->
  shutdown(State).

shutdown(#{user_mod := Mod, user_state := UState}) ->
  eimgui:shutdown(),
  Mod:shutdown(UState),
  ok.

display_errors(Errors) ->
  eimgui:begin_window("Errors", 0),
  [display_error(E) || E <- Errors],
  eimgui:end_window().

display_error({Type, Msg, Trace, Time}) ->
  Red = {255,0,0,1.0},
  eimgui:text("==== " ++ to_string(Time) ++ " ===="),
  eimgui:text_colored(to_string(Type), Red),
  eimgui:text_colored(to_string(Msg), Red),
  eimgui:text(to_string(Trace)).

to_string(Term) ->
  lists:flatten(io_lib_pretty:print(Term)).

%% user_code(State) ->
%%   R = eimgui:button("click"),
%%   case R of
%%     1 ->
%%       1 / 0;
%%     _ ->
%%       ok
%%   end,
%%   State.

receive_message(#{user_mod := UserMod, user_state := UserState}=State) ->
  receive
    {'EXIT', _FromPid, _Reason} ->
      State#{should_close => 1};
    Message ->
      try
        maybe_stop(UserMod:handle_message(Message, UserState), State)
      catch Type:Msg ->
        Trace = erlang:get_stacktrace(),
        report_error(Type, Msg, Trace, State)
      end
    after 0 -> State
  end.

maybe_stop({stop, UState}, State) ->
  State#{should_close => 1, user_state => UState};

maybe_stop({ok, UState}, State) ->
  State#{user_state => UState}.

report_error(Type, Msg, Trace, State) ->
  erlang:display([Msg, Trace]),
  add_error(Type, Msg, Trace, State).

add_error(Type, Err, Trace, #{errors := Errors}=State) ->
  Time = calendar:local_time(),
  State#{
    errors  => [{Type, Err, Trace, Time} | Errors]
  }.

increase_frame_id() ->
  Id = get(?FRAME_ID_KEY),
  put(?FRAME_ID_KEY, Id+1).

frame_id() ->
  get(?FRAME_ID_KEY).

clear_events() ->
  Id = frame_id(),
  case Id rem 5000 of
    0 ->
      Tab = get(?EVENTS_TAB),
      ets:select_delete(Tab, [{{'_', '$1', '_'}, [{'<', '$1', Id}], [true]}]);
    _ -> ok
  end.
